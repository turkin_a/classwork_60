import React, { Component } from 'react';
import './PostForm.css';

class PostForm extends Component {
  render() {
    return (
      <div className="PostForm">
        <input
          type="text"
          className="FormAuthor"
          value={this.props.author}
          onChange={(event) => this.props.changeAuthor(event)}
        />
        <button
          type="button"
          className="FormButton"
          onClick={this.props.click}
        >Add post</button>
        <textarea
          className="FormMessage"
          value={this.props.message}
          onChange={(event) => this.props.changeMessage(event)}
        />
      </div>
    );
  }
}

export default PostForm;