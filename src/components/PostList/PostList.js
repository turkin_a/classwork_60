import React, { Component } from 'react';
import './PostList.css';
import Moment from 'react-moment';
import 'moment-timezone';

class PostList extends Component {
  render() {
    return (
      <div className="PostList">
        {this.props.messageList.map(message => {
          return (
            <div className="Post" key={message._id}>
              <div className="PostTitle">
                <span className="PostAuthor">{message.author}</span> <Moment>{message.datetime}</Moment>
              </div>
              <div className="PostMessage">{message.message}</div>
            </div>);
        }).reverse()}
      </div>
    );
  }
}

export default PostList;