import React, { Component } from 'react';
import './Chat.css';
import PostForm from "../../components/PostForm/PostForm";
import PostList from "../../components/PostList/PostList";

const MESSAGES_URL = 'http://146.185.154.90:8000/messages';

class Chat extends Component {
  state = {
    messageList: [],
    author: '',
    message: ''
  };

  interval = null;

  getMessages = () => {
    let lastMessage = '';

    if (this.state.messageList.length > 0)
      lastMessage = `?datetime=${this.state.messageList[this.state.messageList.length - 1].datetime}`;

    fetch(MESSAGES_URL + lastMessage).then(response => {
      if (response.ok) return response.json();
      throw new Error('Something went wrong with network request');
    }).then(response => {
      let currentList = [...this.state.messageList];

      this.setState(prevState => {
        let newList = currentList.concat(response);
        return prevState.messageList = newList;
      })
    }).catch(error => {
      console.log(error);
    });
  };

  updateMessages = () => {
    this.interval = setInterval(() => this.getMessages(), 5000);
  };

  clearInterval = () => {
    clearInterval(this.interval);
  };

  postMessage = () => {
    const data = new URLSearchParams();
    data.append('author', this.state.author);
    data.append('message', this.state.message);
    const config = {
      method: 'POST',
      body: data
    };

    fetch(MESSAGES_URL, config).then(response => {
      if (response.ok) {
        this.setState({author: '', message: ''});
        this.updateMessages();
        return null;
      }
      throw new Error('Something went wrong with network request');
    }).catch(error => {
      console.log(error);
    });
  };

  handleAuthorInput = (event) => {
    const author = event.target.value;

    this.setState({author});
  };

  handleMessage = (event) => {
    let message = event.target.value;

    this.setState({message});
  };

  componentDidMount() {
    this.getMessages();
    this.updateMessages();
  }

  componentWillUnmount() {
    this.clearInterval();
  }

  render() {
    return (
      <div className="Chat">
        <PostForm
          author={this.state.author}
          changeAuthor={this.handleAuthorInput}
          message={this.state.message}
          changeMessage={this.handleMessage}
          click={this.postMessage}
        />
        <PostList messageList={this.state.messageList}/>
      </div>
    );
  }
}

export default Chat;